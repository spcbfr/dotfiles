* Introduction
Welcome to my doom emacs literate configuration. this file sort of acts as both the configuration and the documentation at the same time. the elisp (emacs variant of lisp) code blocks get /tangled/ to the ~config.el~ file. this is made possible through the doom emacs module *literate*.
now you might be wondering why someone would still use emacs in <insert current year>, well I think this [[yt:bEfYm8sAaQg][video]] by systemcrafters explains this best. now let's get into the actual configuration
* Rudimentary Configuration
first off, let's make the output file run a tiny bit faster
#+begin_src elisp
;;; config.el -*- lexical-binding: t; -*-
#+end_src
** Personal Information
It is useful to have some basic information, apparently this is used by magit, gpg and such
#+begin_src elisp
(setq user-full-name "spcbfr"
      user-mail-address "spcbfr@protonmail.com")
#+end_src
** Window title
I’d like to have just the buffer name, then if applicable the project folder
#+begin_src elisp
(setq frame-title-format
      '(""
        (:eval
         (if (s-contains-p org-roam-directory (or buffer-file-name ""))
             (replace-regexp-in-string
              ".*/[0-9]*-?" "☰ "
              (subst-char-in-string ?_ ?  buffer-file-name))
           "%b"))
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format (if (buffer-modified-p)  " ◉ %s" "  ●  %s") project-name))))))
#+end_src
For example when I open my config file it the window will be titled ~config.org ● doom~ then as soon as I make a change it will become ~config.org ◉ doom~
* Doom Configuration
** Visual Settings
*** Font Face
I am always expermienting with new fonts, so expect this to change very often
#+begin_src elisp
(setq doom-font (font-spec :family "IBM Plex Mono" :size 13) ;; the default general doom font
      doom-unicode-font (font-spec :family "Fira Code" :size 12) ;; Fallback unicode font for icons, glyphs, etc...
      doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 15) ;; the font to use for variable-pitch text
      doom-big-font (font-spec :family "Jetbrains Mono" :size 24)) ;; font used in big-font-mode

#+end_src
I like how italics look (especially with IBM plex mono and with jetbrains mono) so I use them for comments, keywords and other things across my system
#+begin_src elisp
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
#+end_src
*** Dashboard
since the dashboard is its own major-mode, long key cords for quick commands are unnecessary, let's make them simpler
#+begin_src elisp
(map! :map +doom-dashboard-mode-map
      :ne "f" #'find-file
      :ne "r" #'consult-recent-file
      :ne "p" #'doom/open-private-config
      :ne "v" #'vterm/here
      :ne "c" (cmd! (find-file (expand-file-name "config.org" doom-private-dir)))
      :ne "q" #'save-buffers-kill-terminal)
#+end_src
after using doom for a while, the dashboard "useful commands" are no longer useful. we can remove them for a particularly /clean/ and minimal look
#+begin_src elisp
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
#+end_src
*** Line numbers
I never look at line numbers and it makes doom a bit slower so I decided to get rid of them all together
#+begin_src elisp
(setq display-line-numbers-type nil)
#+end_src

* Packages
** pdf-tools
pdf-tools is the package that most people use to open, navigate and read pdf's within emacs. thankfully doom has a module for it so we don't have to write our own config. The only thing I would change is set the midnight-mode (aka dark-mode) to be the default
#+begin_src elisp
(add-hook 'pdf-view-mode-hook (lambda ()
                                (pdf-view-midnight-minor-mode))) ; automatically turns on midnight-mode for pdfs
#+end_src
** rainbow-mode
rainbow-mode displays the actual color for any hex value color.  It's such a nice feature that I wanted it turned on all the time, regardless of what mode I am in, The following line enables rainbow mode for most buffers
#+begin_src elisp
(add-hook! '(text-mode-hook prog-mode-hook conf-mode-hook) #'rainbow-mode)
#+end_src
** emacs-everywhere
emacs everywhere opens an emacs frame for you to type to your heart's content, once you are done, Press ~C-c C-s~ and emacs will paste back what you've written to the focused window
The modeline is not useful to me in the emacs-everywhere popup window. It looks much nicer to hide it.
#+begin_src elisp
(remove-hook 'emacs-everywhere-init-hooks #'hide-mode-line-mode)
#+end_src
semi-center the popup over the target window
#+begin_src elisp
(defadvice! center-emacs-everywhere-in-origin-window (frame window-info)
  :override #'emacs-everywhere-set-frame-position
  (cl-destructuring-bind (x y width height)
      (emacs-everywhere-window-geometry window-info)
    (set-frame-position frame
                        (+ x (/ width 2) (- (/ width 2)))
                        (+ y (/ height 2)))))
#+end_src
and finally, make emacs-everywhere easier to match with an xmonad rule
#+begin_src elisp
(setq emacs-everywhere-frame-name-format "emacs-everywhere")
#+end_src
** Org-mode, the killer feature of emacs
org-mode is a quite powerful task-management and note-taking tool, in fact, I am using org-mode right now to write this literate configuration, orgmode is available as a doom module that is enabled by default

Let's start by setting the org-directory
#+begin_src elisp
(setq org-directory "~/docs/org")
#+end_src
*** org-roam
org-roam is a package that extends the default org functionality, allowing things like daily notes, and backlinks, similar to [[https:roamresearch.com/][Roam Research]] which inspired org-roam. the only thing that is really required for org-roam is the directory in which org-roam files are stored
#+begin_quote
note that org-roam files, are just org-mode files that have an id making org-roam able to organise them in neat ways
#+end_quote
we also need to specifiy a directory for the org-roam dailies, this is relative to ~org-roam-directory~
#+begin_src elisp
(setq org-roam-dailies-directory "daily")
#+end_src
I like to add a timestamp to each org-roam-dailies entry, just to be a /little/ bit more organised
#+begin_src elisp
(setq org-roam-dailies-capture-templates
      '(("d" "default" entry "* %<%I:%M %p>: %?"
         :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
#+end_src
** which-key
which-key is the thing that pops up whenever you start typing a key-cord, it is pretty useful but it takes a long time to show up. let's speed things up a bit
#+begin_src elisp
(setq which-key-idle-delay 0.5) ;; I need the help, I really do
#+end_src
having the ~evil-~ prefix before every evil-mode command is a bit too verbose, I prefer to keep things simple
#+begin_src elisp
(setq which-key-allow-multiple-replacements t)
(after! which-key
  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))
   ))
#+end_src
** Ebooks with nov.el and calibre

#+begin_src elisp
(use-package! calibredb
  :commands calibredb
  :config
  (setq calibredb-root-dir "~/docs/ebooks"
        calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (map! :map calibredb-show-mode-map
        :ne "?" #'calibredb-entry-dispatch
        :ne "o" #'calibredb-find-file
        :ne "O" #'calibredb-find-file-other-frame
        :ne "V" #'calibredb-open-file-with-default-tool
        :ne "s" #'calibredb-set-metadata-dispatch
        :ne "e" #'calibredb-export-dispatch
        :ne "q" #'calibredb-entry-quit
        :ne "." #'calibredb-open-dired
        :ne [tab] #'calibredb-toggle-view-at-point
        :ne "M-t" #'calibredb-set-metadata--tags
        :ne "M-a" #'calibredb-set-metadata--author_sort
        :ne "M-A" #'calibredb-set-metadata--authors
        :ne "M-T" #'calibredb-set-metadata--title
        :ne "M-c" #'calibredb-set-metadata--comments)
  (map! :map calibredb-search-mode-map
        :ne [mouse-3] #'calibredb-search-mouse
        :ne "RET" #'calibredb-find-file
        :ne "?" #'calibredb-dispatch
        :ne "a" #'calibredb-add
        :ne "A" #'calibredb-add-dir
        :ne "c" #'calibredb-clone
        :ne "d" #'calibredb-remove
        :ne "D" #'calibredb-remove-marked-items
        :ne "j" #'calibredb-next-entry
        :ne "k" #'calibredb-previous-entry
        :ne "l" #'calibredb-virtual-library-list
        :ne "L" #'calibredb-library-list
        :ne "n" #'calibredb-virtual-library-next
        :ne "N" #'calibredb-library-next
        :ne "p" #'calibredb-virtual-library-previous
        :ne "P" #'calibredb-library-previous
        :ne "s" #'calibredb-set-metadata-dispatch
        :ne "S" #'calibredb-switch-library
        :ne "o" #'calibredb-find-file
        :ne "O" #'calibredb-find-file-other-frame
        :ne "v" #'calibredb-view
        :ne "V" #'calibredb-open-file-with-default-tool
        :ne "." #'calibredb-open-dired
        :ne "b" #'calibredb-catalog-bib-dispatch
        :ne "e" #'calibredb-export-dispatch
        :ne "r" #'calibredb-search-refresh-and-clear-filter
        :ne "R" #'calibredb-search-clear-filter
        :ne "q" #'calibredb-search-quit
        :ne "m" #'calibredb-mark-and-forward
        :ne "f" #'calibredb-toggle-favorite-at-point
        :ne "x" #'calibredb-toggle-archive-at-point
        :ne "h" #'calibredb-toggle-highlight-at-point
        :ne "u" #'calibredb-unmark-and-forward
        :ne "i" #'calibredb-edit-annotation
        :ne "DEL" #'calibredb-unmark-and-backward
        :ne [backtab] #'calibredb-toggle-view
        :ne [tab] #'calibredb-toggle-view-at-point
        :ne "M-n" #'calibredb-show-next-entry
        :ne "M-p" #'calibredb-show-previous-entry
        :ne "/" #'calibredb-search-live-filter
        :ne "M-t" #'calibredb-set-metadata--tags
        :ne "M-a" #'calibredb-set-metadata--author_sort
        :ne "M-A" #'calibredb-set-metadata--authors
        :ne "M-T" #'calibredb-set-metadata--title
        :ne "M-c" #'calibredb-set-metadata--comments))
#+end_src
#+begin_src elisp
(use-package! nov
  :mode ("\\.epub\\'" . nov-mode)
  :config
  (map! :map nov-mode-map
        :n "RET" #'nov-scroll-up)

  (defun doom-modeline-segment--nov-info ()
    (concat
     " "
     (propertize
      (cdr (assoc 'creator nov-metadata))
      'face 'doom-modeline-project-parent-dir)
     " "
     (cdr (assoc 'title nov-metadata))
     " "
     (propertize
      (format "%d/%d"
              (1+ nov-documents-index)
              (length nov-documents))
      'face 'doom-modeline-info)))

  (advice-add 'nov-render-title :override #'ignore)

  (defun +nov-mode-setup ()
    (face-remap-add-relative 'variable-pitch
                             :family "Merriweather"
                             :height 1.4
                             :width 'semi-expanded)
    (face-remap-add-relative 'default :height 1.3)
    (setq-local line-spacing 0.2
                next-screen-context-lines 4
                shr-use-colors nil)
    (require 'visual-fill-column nil t)
    (setq-local visual-fill-column-center-text t
                visual-fill-column-width 81
                nov-text-width 80)
    (visual-fill-column-mode 1)
    (hl-line-mode -1)

    (add-to-list '+lookup-definition-functions #'+lookup/dictionary-definition)

    (setq-local mode-line-format
                `((:eval
                   (doom-modeline-segment--workspace-name))
                  (:eval
                   (doom-modeline-segment--window-number))
                  (:eval
                   (doom-modeline-segment--nov-info))
                  ,(propertize
                    " %P "
                    'face 'doom-modeline-buffer-minor-mode)
                  ,(propertize
                    " "
                    'face (if (doom-modeline--active) 'mode-line 'mode-line-inactive)
                    'display `((space
                                :align-to
                                (- (+ right right-fringe right-margin)
                                   ,(* (let ((width (doom-modeline--font-width)))
                                         (or (and (= width 1) 1)
                                             (/ width (frame-char-width) 1.0)))
                                       (string-width
                                        (format-mode-line (cons "" '(:eval (doom-modeline-segment--major-mode))))))))))
                  (:eval (doom-modeline-segment--major-mode)))))

  (add-hook 'nov-mode-hook #'+nov-mode-setup))
#+end_src
